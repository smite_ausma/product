# Junior Developers test

```
Ausma Šmite, ausma.shmite@gmail.com
```

## Install
```
$ composer install
```

## Project setup
```
1. Create database 'product' (MySQl)
2. Import dump 'product.sql' from root directory to create a new database.
```

## Notes

```
Project uses php, vanilla Javascript, SCSS.
SCSS compiler: scssphp.
```

```
Project tested on:
    - Brave (Version 1.9.76);
    - Firefox (Version 76.0.1).
```