-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 31, 2020 at 09:42 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `product`
--

-- --------------------------------------------------------

--
-- Table structure for table `attribute`
--

CREATE TABLE `attribute` (
  `attribute_id` int(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `attribute`
--

INSERT INTO `attribute` (`attribute_id`, `name`, `description`) VALUES
(1, 'DVD-Disc', 'Please provide size in MB'),
(2, 'Book', 'Please provide size in kg'),
(3, 'Furniture', 'Please provide dimensions in HxWxL format');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `sku` varchar(9) NOT NULL,
  `name` varchar(100) NOT NULL,
  `price` decimal(8,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`sku`, `name`, `price`) VALUES
('3TNDFW3FN', 'Roadside Picnic', '47.00'),
('9PSMAQ1PB', 'Chair', '120.45'),
('DAJCTYTOQ', 'Bed', '555.99'),
('EH48TJ283', 'Table', '180.00'),
('EM2EE4Y89', 'Verbatim DISC', '20.00'),
('EZYZLA346', 'Stalker', '14.05'),
('KUO06OA99', 'War and Peace', '21.05'),
('MYHFC8SHF', 'Storage Cabinet', '450.65'),
('N6TUXPN6M', 'Maxell DISC', '45.28'),
('UBR3WXN62', 'The Decameron', '57.54'),
('WLNSU0O5F', 'Maxell DISC', '15.00'),
('Y6VDYWC88', 'Acme DISC', '35.49');

-- --------------------------------------------------------

--
-- Table structure for table `product_type`
--

CREATE TABLE `product_type` (
  `sku` varchar(9) NOT NULL,
  `type_id` int(10) NOT NULL,
  `value` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_type`
--

INSERT INTO `product_type` (`sku`, `type_id`, `value`) VALUES
('Y6VDYWC88', 1, '500'),
('EM2EE4Y89', 1, '600'),
('WLNSU0O5F', 1, '700'),
('N6TUXPN6M', 1, '540'),
('KUO06OA99', 2, '1'),
('UBR3WXN62', 2, '2'),
('3TNDFW3FN', 2, '1'),
('EZYZLA346', 2, '3'),
('9PSMAQ1PB', 3, '58'),
('9PSMAQ1PB', 4, '50'),
('9PSMAQ1PB', 5, '70'),
('EH48TJ283', 3, '180'),
('EH48TJ283', 4, '80'),
('EH48TJ283', 5, '50'),
('DAJCTYTOQ', 3, '185'),
('DAJCTYTOQ', 4, '95'),
('DAJCTYTOQ', 5, '14'),
('MYHFC8SHF', 3, '200'),
('MYHFC8SHF', 4, '150'),
('MYHFC8SHF', 5, '34');

-- --------------------------------------------------------

--
-- Table structure for table `type`
--

CREATE TABLE `type` (
  `type_id` int(10) NOT NULL,
  `attribute_id` int(10) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `type`
--

INSERT INTO `type` (`type_id`, `attribute_id`, `name`) VALUES
(1, 1, 'Size'),
(2, 2, 'Weight'),
(3, 3, 'Height'),
(4, 3, 'Width'),
(5, 3, 'Length');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attribute`
--
ALTER TABLE `attribute`
  ADD PRIMARY KEY (`attribute_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`sku`);

--
-- Indexes for table `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`type_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attribute`
--
ALTER TABLE `attribute`
  MODIFY `attribute_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `type`
--
ALTER TABLE `type`
  MODIFY `type_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
