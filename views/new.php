<?php
include 'templates/head.php';
?>
<main class="main" role="main" id="add">
    <?php
    include 'templates/navigation.php';
    ?>
  <section class="section__head">
    <h1>Product Add</h1>
    <label class="btn__submit" for="submit_form" tabindex="0" id="save">Save</label>
  </section>
  <section class="section__form">
    <form name="add_product" class="form" id="add_product">
      <div class="form__section">
        <label for="sku">SKU</label>
        <input type="text" id="sku" name="sku" autocomplete="off">
      </div>
      <div class="form__section">
        <label for="name">Name</label>
        <input type="text" id="name" name="name" autocomplete="off">
      </div>
      <div class="form__section">
        <label for="price">Price</label>
        <input type="text" id="price" name="price" autocomplete="off">
      </div>
      <div class="form__section">
        <label for="attribute">Type Switcher</label>
        <select name="attribute" id="attribute">
          <option value="" selected disabled>Type Switcher</option>
          <option value="1">DVD Disc</option>
          <option value="2">Book</option>
          <option value="3">Furniture</option>
        </select>
      </div>
      <div id="hidden" class="hidden">
      </div>
      <input type="submit" id="submit_form" class="hidden">
    </form>
  </section>
  <section class="section__alert" id="alerts">
  </section>
  <b class="success" id="success">Product added!</b>
</main>
<?php
include 'templates/foot.php';
?>
