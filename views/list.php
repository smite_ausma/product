<?php
include 'templates/head.php';
?>
<main class="main" role="main" id="list">
    <?php
    include 'templates/navigation.php';
    ?>
  <section class="section__head">
    <h1>Product List</h1>
    <form id="product_delete" name="product_delete">
      <label for="delete_selected"></label>
      <select name="delete_selected" id="delete_selected">
        <option value="" id="delete_all">Mass Delete Action</option>
      </select>
      <button type="submit">Apply</button>
    </form>
  </section>
  <section class="section__list">
      <?php if (isset($data) && !empty($data)): ?>
          <?php foreach ($data as $product) : ?>
          <article class="article">
            <input class="delete" id="<?= $product['sku'] . '_product' ?>" type="checkbox" name="delete"
                   value="<?= $product['sku'] ?>">
            <label for="<?= $product['sku'] . '_product' ?>"></label>
            <div class="article__text">
              <p><?= $product['sku'] ?></p>
              <h2><?= $product['name'] ?></h2>
              <p><?= $product['price'] ?> $</p>
              <p><?= $product['value'] ?></p>
            </div>
          </article>
          <?php endforeach; ?>
      <?php endif; ?>
  </section>
</main>
<?php
include 'templates/foot.php';
?>
