<?php


namespace App;


use PDO;
use PDOException;

class Database
{
    private $host = 'localhost';
    private $user = 'root';
    private $password = '';
    private $db = 'product';

    protected function connectDatabase()
    {
        $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->db;

        try {
            return new PDO($dsn, $this->user, $this->password);
        } catch (PDOException $e) {
            print 'Error!: ' . $e->getMessage() . '<br/>';
            die();
        }
    }
}