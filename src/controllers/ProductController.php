<?php


namespace App\controllers;


use App\core\Controller;

/**
 * Class ProductController
 * @package App\controllers
 */
class ProductController extends Controller
{
    /**
     * @var
     */
    private $data;
    /**
     * @var string
     */
    private $title = 'Product List';
    /**
     * @var
     */
    private $view;
    /**
     * @var
     */
    private $model;

    /**
     * ProductController constructor.
     * @param $view
     */
    public function __construct($view)
    {
        $this->setModel();
        $this->getProducts();
        $this->setView($view);
        $this->view();
    }

    /**
     *
     */
    private function setModel()
    {
        $this->model = $this->getModel('Product');
    }

    /**
     * @param $view
     */
    private function setView($view)
    {
        $this->view = 'views/' . $view . '.php';
    }

    /**
     *
     */
    private function getProducts()
    {
        $this->data = $this->model->selectAll();
    }

    /**
     *
     */
    private function view()
    {
        $this->renderView($this->data, $this->title, $this->view);
    }

}