<?php

namespace App\core;


/**
 * Class App
 * @package App\core
 */
class App
{

    /**
     * @var
     */
    private $controller;
    /**
     * @var
     */
    private $type;
    /**
     * @var
     */
    private $action;
    /**
     * @var
     */
    private $method;
    /**
     * @var
     */
    private $view;
    /**
     * @var \string[][][]
     */
    private $controllers = array(
        array('method' => array('add' => 'RequestController', 'delete' => 'RequestController', 'attributes' => 'RequestController')),
        array('view' => array('home' => 'ProductController', 'list' => 'ProductController', 'new' => 'ProductController', 'error' => 'ErrorController')),
    );
    /**
     * @var \string[][][]
     */
    private $actions = array(array('method' => array('add', 'delete', 'attributes')), array('view' => array('list', 'new')));

    /**
     * App constructor.
     */
    public function __construct()
    {
        $url = $this->parseUrl();
        $this->handleUrl($url);
        $this->handleActions();
        $this->handleObject($this->view, $this->method, $this->controller);
    }

    /**
     * @return false|string[]
     *
     */
    private function parseUrl()
    {
        if (isset($_SERVER['REQUEST_URI'])) {
            $trim = trim($_SERVER['REQUEST_URI'], '/');
            $filter = filter_var($trim, FILTER_SANITIZE_URL);
            $lower = strtolower($filter);
            return explode('/', $lower);
        }

    }

    /**
     * @param $url
     * Set type.
     * Set action.
     */
    private function handleUrl($url)
    {
        if (!empty($url[1])) {
            foreach ($this->actions as $arr) {
                $values = array_values($arr);
                foreach ($values as $value) {
                    $key = array_search($url[1], $value);
                    if ($key > -1) {
                        $this->setType(array_keys($arr)[0]);
                        $this->setAction($value[$key]);
                    }
                }
            }
        } else {
            $this->setType('view');
            $this->setAction('home');
        }
        if (empty($this->type) && empty($this->action)) {
            $this->setType('view');
            $this->setAction('error');
        }
    }

    /**
     *
     * Differentiate between method and view.
     */
    private function handleActions()
    {
        foreach ($this->controllers as $arr) {
            $keys = array_keys($arr);
            $key = array_search($this->type, $keys);
            if ($key > -1) {
                if (array_keys($arr)[0] === 'method') {
                    $this->setMethod($this->action);
                    $values = array_values($arr)[0];
                    if ($values[$this->action]) {
                        $this->setController($values[$this->action]);
                    }
                } elseif (array_keys($arr)[0] === 'view') {
                    $this->setView($this->action);
                    $values = array_values($arr)[0];
                    if ($values[$this->action]) {
                        $this->setController($values[$this->action]);
                    }
                }
            }
        }
    }

    /**
     * @param $view
     * @param $method
     * @param $controller
     *
     * Check if controller exists.
     * Set method or view.
     */
    private function handleObject($view, $method, $controller)
    {
        if (file_exists('src/controllers/' . $controller . '.php')) {
            if (!empty($method)) {
                $this->instantiateObject($method);
            } elseif (!empty($view) && file_exists('views/' . $view . '.php')) {
                $this->instantiateObject($view);
            }
        }
    }

    /**
     * @param $type
     */
    private function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @param $action
     */
    private function setAction($action)
    {
        $this->action = $action;
    }

    /**
     * @param $method
     */
    private function setMethod($method)
    {
        $this->method = $method;
    }

    /**
     * @param $controller
     */
    private function setController($controller)
    {
        $this->controller = $controller;
    }

    /**
     * @param $view
     */
    private function setView($view)
    {
        $this->view = $view;
    }

    /**
     * @param $value
     * Instantiate object.
     */
    private function instantiateObject($value)
    {
        $this->controller = 'App\\controllers\\' . $this->controller;
        $obj = new $this->controller($value);

    }

}