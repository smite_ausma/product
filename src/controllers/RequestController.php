<?php


namespace App\controllers;


use App\core\Controller;

/**
 * Class RequestController
 * @package App\controllers
 */
class RequestController extends Controller
{
    /**
     * @var
     */
    private $model;
    /**
     * @var
     */
    private $controller;
    /**
     * @var
     */
    private $view;

    /**
     * RequestController constructor.
     * @param $method
     */
    public function __construct($method)
    {
        $this->setController();
        $this->setView();
        $this->setModel();
        $this->$method();
    }

    /**
     *
     */
    private function setModel()
    {
        $this->model = $this->getModel('Product');
    }

    /**
     *
     */
    private function setView()
    {
        $this->view = 'error';
    }

    /**
     *
     */
    private function setController()
    {
        $this->controller = 'ErrorController';
    }

    /**
     * Delete Product
     */
    private function delete()
    {
        if (isset($_POST['delete_selected']) && !(empty($_POST['delete_selected']))) {
            $data = $_POST['delete_selected'];
            $this->model->deleteProducts($data);
        } else {
            $this->showErrorPage();
        }
    }

    /**
     * Get Product attributes
     */
    private function attributes()
    {
        if (isset($_POST['attr_id'])) {
            $result = $this->model->getAttributes($_POST['attr_id']);

            echo json_encode($result);
        } else {
            $this->showErrorPage();
        }
    }

    /**
     * Add Product
     */
    private function add()
    {

        if (empty($_POST)) {
            $this->showErrorPage();
        } else {
            $errors = array();
            if (isset($_POST['sku']) && !empty($_POST['sku'])) {
                $this->model->setSku($_POST['sku']);
                $exist = $this->model->checkUniqueSku();
                if ($exist) {
                    $error = 'Sku must be unique!';
                    $errors[] = $error;
                }
                $regex_err = $this->model->validateSku();
                if ($regex_err) {
                    $error = 'Sku must be 9 characters long and contain uppercase only letters and/or numbers.';
                    $errors[] = $error;
                }
            } else {
                $error = 'Please provide sku!';
                $errors[] = $error;
            }
            if (isset($_POST['name']) && !empty($_POST['name'])) {
                $this->model->setName($_POST['name']);
                $length_err = $this->model->validateName();
                if ($length_err) {
                    $error = 'Name must be no longer than 100 characters.';
                    $errors[] = $error;
                }
            } else {
                $error = 'Please provide name!';
                $errors[] = $error;
            }
            if (isset($_POST['price']) && !empty($_POST['price'])) {
                $this->model->setPrice($_POST['price']);
                $reg_err = $this->model->validatePrice();
                if ($reg_err) {
                    $error = 'Price format examples (numbers only): 0.99, 1.00, 9999.00.';
                    $errors[] = $error;
                }
            } else {
                $error = 'Please provide price!';
                $errors[] = $error;
            }
            if (!empty($_POST['attribute'])) {
                $this->model->setAttribute($_POST['attribute']);
                switch ($_POST['attribute']) {
                    case 1:
                        if (isset($_POST['type_0']) && !empty($_POST['type_0'])) {
                            $this->model->setSize($_POST['type_0']);
                            $reg_err = $this->model->validateSize();
                            if ($reg_err) {
                                $error = 'Size must be integer only.';
                                $errors[] = $error;
                            }
                        } else {
                            $error = 'Please provide size!';
                            $errors[] = $error;
                        }
                        break;
                    case 2:
                        if (isset($_POST['type_0']) && !empty($_POST['type_0'])) {
                            $this->model->setWeight($_POST['type_0']);
                            $reg_err = $this->model->validateWeight();
                            if ($reg_err) {
                                $error = 'Weight must be integer only.';
                                $errors[] = $error;
                            }
                        } else {
                            $error = 'Please provide weight!';
                            $errors[] = $error;
                        }
                        break;
                    case 3:
                        if (isset($_POST['type_0']) && !empty($_POST['type_0'])) {
                            $this->model->setHeight($_POST['type_0']);
                            $reg_err = $this->model->validateHeight();
                            if ($reg_err) {
                                $error = 'Height must be integer only.';
                                $errors[] = $error;
                            }
                        } else {
                            $error = 'Please provide height!';
                            $errors[] = $error;
                        }
                        if (isset($_POST['type_1']) && !empty($_POST['type_1'])) {
                            $this->model->setWidth($_POST['type_1']);
                            $reg_err = $this->model->validateWidth();
                            if ($reg_err) {
                                $error = 'Width must be integer only.';
                                $errors[] = $error;
                            }
                        } else {
                            $error = 'Please provide width!';
                            $errors[] = $error;
                        }
                        if (isset($_POST['type_2']) && !empty($_POST['type_2'])) {
                            $this->model->setLength($_POST['type_2']);
                            $reg_err = $this->model->validateLength();
                            if ($reg_err) {
                                $error = 'Length must be integer only.';
                                $errors[] = $error;
                            }
                        } else {
                            $error = 'Please provide length!';
                            $errors[] = $error;
                        }
                        break;
                }

            } else {
                $error = 'Please provide special attributes!';
                $errors[] = $error;
            }
            if ($errors) {
                echo json_encode($errors);
            } else {
                $this->model->addProduct();
                echo json_encode($errors);
            }
        }
    }

    /**
     * Show error page if methods not set
     */
    private function showErrorPage()
    {
        if (file_exists('src/controllers/' . $this->controller . '.php')) {
            $this->controller = 'App\\controllers\\' . $this->controller;
            $obj = new $this->controller($this->view);
        }
    }


}