<?php
//Hardcoded type_id

namespace App\models;


use App\Database;
use PDO;

/**
 * Class Product
 * @package App\models
 */
class Product extends Database
{
    /**
     * @var
     */
    private $sku;
    /**
     * @var
     */
    private $name;
    /**
     * @var
     */
    private $price;
    /**
     * @var
     */
    private $attribute;
    /**
     * @var
     */
    private $size;
    /**
     * @var
     */
    private $weight;
    /**
     * @var
     */
    private $height;
    /**
     * @var
     */
    private $width;
    /**
     * @var
     */
    private $length;

    /**
     * @return array
     * Return array with product sku, product name, product price, product type value, product type id.
     */
    public function selectAll()
    {
        $sql = 'SELECT product.sku, product.name, product.price, product_type.value, product_type.type_id
                FROM product
                LEFT JOIN product_type
                ON product.sku = product_type.sku';
        $stmt = $this->connectDatabase()->query($sql);
        $items = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $arr = array();
        foreach ($items as $item) {
            switch ($item['type_id']) {
                case 1:
                    $item['value'] .= ' MB';
                    break;
                case 2:
                    $item['value'] .= ' KG';
                    break;
            }

            if (isset($arr[$item['sku']])) {
                $arr[$item['sku']]['sku'] = $item['sku'];
                $arr[$item['sku']]['name'] = $item['name'];
                $arr[$item['sku']]['price'] = $item['price'];
                $arr[$item['sku']]['value'] .= 'x' . $item['value'];
            } else {
                $arr[$item['sku']] = $item;
            }
        }
        $arr = array_values($arr);
        return $arr;
    }

    /**
     * @param $id
     * @return array
     * Return array of product attributes that includes attribute id, attribute description and type name.
     */
    public function getAttributes($id)
    {
        $sql = 'SELECT attribute.attribute_id, attribute.description, type.name
                FROM attribute
                LEFT JOIN type
                ON attribute.attribute_id = type.attribute_id WHERE attribute.attribute_id = ?';
        $conn = $this->connectDatabase();
        $stmt = $conn->prepare($sql);
        $req = filter_var($id, FILTER_SANITIZE_NUMBER_INT);
        $stmt->execute([$req]);
        $items = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $arr = array();
        foreach ($items as $item) {
            if (isset($arr[$item['attribute_id']])) {
                $arr[$item['attribute_id']]['attribute_id'] = $item['attribute_id'];
                $arr[$item['attribute_id']]['description'] = $item['description'];
                $arr[$item['attribute_id']]['name'] .= ',' . $item['name'];
            } else {
                $arr[$item['attribute_id']] = $item;
            }
        }
        $arr = array_values($arr);
        return $arr;
    }

    /**
     * @param $id
     * Delete one ore more products based on product sku.
     */
    public function deleteProducts($id)
    {
        $sql = 'DELETE product, product_type FROM product LEFT JOIN product_type ON product.sku = product_type.sku WHERE product_type.sku IN ( ? )';
        $conn = $this->connectDatabase();
        $stmt = $conn->prepare($sql);
        $ids = filter_var($id, FILTER_SANITIZE_STRING);
        $arr = explode(',', $ids);
        foreach ($arr as $id) {
            $stmt->execute([$id]);
        }
    }

    /**
     * @return bool
     * Add product sku, product name, product price, product type value, product type id.
     */
    public function addProduct()
    {
        $sql = 'INSERT INTO product (sku, name, price) VALUES (?, ?, ?)';
        $conn = $this->connectDatabase();
        $stmt = $conn->prepare($sql);
        $sku = filter_var($this->sku, FILTER_SANITIZE_STRING);
        $name = filter_var($this->name, FILTER_SANITIZE_STRING);
        $price = filter_var($this->price, FILTER_SANITIZE_STRING);
        $stmt->execute([$sku, $name, $price]);

        switch ($this->attribute) {
            case 1:
                $sql = 'INSERT INTO product_type (sku, type_id, value) VALUES (?, ?, ?)';
                $conn = $this->connectDatabase();
                $stmt = $conn->prepare($sql);
                $size = filter_var($this->size, FILTER_SANITIZE_NUMBER_INT);
                $stmt->execute([$sku, 1, $size]);
                break;
            case 2:
                $sql = 'INSERT INTO product_type (sku, type_id, value) VALUES (?, ?, ?)';
                $conn = $this->connectDatabase();
                $stmt = $conn->prepare($sql);
                $weight = filter_var($this->weight, FILTER_SANITIZE_NUMBER_INT);
                $stmt->execute([$sku, 2, $weight]);
                break;
            case 3:
                $sql = 'INSERT INTO product_type (sku, type_id, value) VALUES (?, ?, ?)';
                $conn = $this->connectDatabase();
                $stmt = $conn->prepare($sql);
                $value = $this->height . 'x' . $this->width . 'x' . $this->length;
                $dimensions = filter_var($value, FILTER_SANITIZE_STRING);
                $stmt->execute([$sku, 3, $dimensions]);
                break;
        }

        return true;
    }

    /**
     * @param $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @param $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @param $attribute
     */
    public function setAttribute($attribute)
    {
        $this->attribute = $attribute;
    }

    /**
     * @param $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }

    /**
     * @param $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * @param $height
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }

    /**
     * @param $width
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    /**
     * @param $length
     */
    public function setLength($length)
    {
        $this->length = $length;
    }

    /**
     * @return bool
     * Check if product sku is unique before adding to db
     */
    public function checkUniqueSku()
    {
        $sql = 'SELECT sku FROM product WHERE sku = ?';
        $conn = $this->connectDatabase();
        $stmt = $conn->prepare($sql);
        $sku = filter_var($this->sku, FILTER_SANITIZE_STRING);
        $stmt->execute([$sku]);
        return !($stmt->rowCount() === 0);
    }

    /**
     * @return bool
     */
    public function validateSku()
    {
        return !(preg_match('/[0-9A-Z]{9}$/', $this->sku));
    }

    /**
     * @return bool
     */
    public function validateName()
    {
        return !(strlen($this->name) <= 100);
    }

    /**
     * @return bool
     */
    public function validatePrice()
    {
        return !(preg_match('/^(?!^0\.00$)(([1-9][\d]{0,6})|([0]))\.[\d]{2}$/', $this->price));
    }

    /**
     * @return bool
     */
    public function validateSize()
    {
        return !(preg_match('/^[0-9]*$/', $this->size));
    }

    /**
     * @return bool
     */
    public function validateWeight()
    {
        return !(preg_match('/^[0-9]*$/', $this->weight));
    }

    /**
     * @return bool
     */
    public function validateHeight()
    {
        return !(preg_match('/^[0-9]*$/', $this->height));
    }

    /**
     * @return bool
     */
    public function validateWidth()
    {
        return !(preg_match('/^[0-9]*$/', $this->width));
    }

    /**
     * @return bool
     */
    public function validateLength()
    {
        return !(preg_match('/^[0-9]*$/', $this->length));
    }

}