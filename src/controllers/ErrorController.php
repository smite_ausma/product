<?php


namespace App\controllers;


use App\core\Controller;

/**
 * Class ErrorController
 * @package App\controllers
 */
class ErrorController extends Controller
{
    /**
     * @var string
     */
    private $title = 'Product';
    /**
     * @var
     */
    private $view;
    /**
     * @var string
     */
    private $data = '404 Not Found';

    /**
     * ErrorController constructor.
     * @param $view
     */
    public function __construct($view)
    {
        $this->setView($view);
        $this->view();
    }

    /**
     * @param $view
     */
    private function setView($view)
    {
        $this->view = 'views/' . $view . '.php';
    }

    /**
     *
     */
    private function view()
    {
        header('HTTP/1.0 404 Not Found');
        $this->renderView($this->data, $this->title, $this->view);
        exit();
    }

}