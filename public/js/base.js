const list = document.getElementById('list')
const add = document.getElementById('add')

if (list) {
  const formDelete = document.getElementById('product_delete')
  const checkboxes = document.querySelectorAll('.delete')
  const formDeleteOption = document.getElementById('delete_all')

  /*
  Check if checkboxes checked.
  Add checked to array OR remove from array if user unchecks.
  */
  let arr = []
  checkboxes.forEach(x => {
    x.addEventListener('click', function () {
      if (this.checked) {
        arr.push(this.value)
      } else {
        let index = arr.indexOf(this.value)
        if (index > -1) {
          arr.splice(index, 1)
        }
      }
    })
  })

  /*
  Use Fetch API for request.
  No support in IE.
  Ajax for IE.
  */
  formDelete.addEventListener('submit', (e) => {
    e.preventDefault()
    if (!arr.length) {
      alert('Please choose one or more items!')
    } else {
      if (confirm('Are you sure you want to delete?')) {
        formDeleteOption.value = arr
        let formData = new FormData(formDelete)
        fetch('delete', {
          method: 'POST',
          body: formData,
        }).
          then(response => console.log(response)).
          then(data => {
            console.log(data)
            window.location.reload()
          }).
          catch(e => console.error(e))
      }
    }
  })
}

if (add) {
  const typeOption = document.getElementById('attribute')
  const hiddenField = document.getElementById('hidden')
  const addForm = document.getElementById('add_product')
  const alertField = document.getElementById('alerts')
  const success = document.getElementById('success')

  /*
  Type Switcher functionality.
  Dynamic helper functionality.
  */
  typeOption.addEventListener('change', () => {
      hiddenField.innerHTML = ''
      alertField.innerHTML = ''
      let formData = new FormData()
      formData.append('attr_id', typeOption.value)
      if (typeOption.value) {
        fetch('attributes', {
          method: 'POST',
          body: formData,
        }).
          then(response => response.json()).
          then(data => {

            data.forEach(x => {
              let attributes = x['name'].split(',')
              for (let i = 0; i < attributes.length; i++) {
                let attr = attributes[i]
                let div = document.createElement('div')
                div.classList.add('form__section')
                let label = document.createElement('label')
                let input = document.createElement('input')
                input.type = 'text'
                input.name = `type_${i}`
                input.autocomplete = 'off'
                label.innerText = attr
                div.appendChild(label)
                div.appendChild(input)
                hiddenField.appendChild(div)
              }
              let text = document.createElement('p')
              text.innerText = x['description']
              hiddenField.appendChild(text)
            })
            hiddenField.classList.add('active')
          }).
          catch(e => console.error(e))
      }
    },
  )

  /*
  Submit data to add product.
  Display alerts.
  */
  addForm.addEventListener('submit', (e) => {
    e.preventDefault()
    let formData = new FormData(addForm)
    alertField.innerHTML = ''
    fetch('add', {
      method: 'POST',
      body: formData,
    }).
      then(response => response.json()).
      then(data => {
        if (data.length > 0) {
          success.classList.remove('active')
          data.forEach(x => {
            let text = document.createElement('p')
            text.classList.add('error')
            text.innerText = x
            alertField.appendChild(text)
          })
        } else {
          addForm.reset()
          hiddenField.innerHTML = ''
          success.classList.add('active')
        }
      }).
      catch(e => console.error(e))
  })

}
