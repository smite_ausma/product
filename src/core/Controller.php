<?php


namespace App\core;


/**
 * Class Controller
 * @package App\core
 */
class Controller
{
    /**
     * @param $model
     * @return mixed
     */
    protected function getModel($model)
    {
        $class = 'App\\models\\' . $model;
        return new $class;
    }

    /**
     * @param $data
     * @param $title
     * @param $view
     */
    protected function renderView($data, $title, $view)
    {
        require_once $view;
    }

}