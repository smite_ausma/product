<?php
include 'templates/head.php';
?>
<main class="main">
    <?php
    include 'templates/navigation.php';
    ?>
    <?php if (isset($data) && !empty($data)) : ?>
      <h1><?= $data ?></h1>
    <?php endif; ?>
</main>
<?php
include 'templates/foot.php';
?>
